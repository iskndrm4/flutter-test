import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mobile_legend/main.dart';
import 'package:mobile_legend/models/user_model.dart';

class ProfileProvider extends ChangeNotifier {
  late UserModel _userModel;
  UserModel get userModel => _userModel;

  bool _isLoading = true;
  bool get isLoading => _isLoading;

  Future<void> getUserDataFromFirestore() async {
    _isLoading = true;
    notifyListeners();

    final userId = prefs.getString('user_id');

    DocumentSnapshot<Map<String, dynamic>> snapshot =
        await FirebaseFirestore.instance.collection('users').doc(userId).get();

    if (snapshot.exists) {
      _userModel = UserModel.fromJson(snapshot.data()!);
    } else {
      throw Exception('User data not found!');
    }

    _isLoading = false;
    notifyListeners();
  }
}
