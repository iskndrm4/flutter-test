import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:mobile_legend/models/favorite/hero_favorite.dart';
import 'package:mobile_legend/models/hero_model.dart';
import 'package:mobile_legend/services/hero_service_dio.dart';

class HeroProvider with ChangeNotifier {
  bool _isLoading = true;
  bool get isLoading => _isLoading;

  bool _isLoadingDetail = true;
  bool get isLoadingDetail => _isLoadingDetail;

  List<HeroModel> _heroes = [];
  List<HeroModel> get heroes => _heroes;

  late HeroModel _hero;
  HeroModel get hero => _hero;

  int clickedIndex = 0;

  final Box<HeroFavorite> heroFavoritesBox =
      Hive.box<HeroFavorite>('heroFavorites');

  void toggleTapState(int index, String role) {
    clickedIndex = index;
    notifyListeners();
    if (role == 'Semua') {
      getHeroes();
    } else {
      getHeroesByRole(role);
    }
  }

  Future<void> getHeroes() async {
    _isLoading = true;
    notifyListeners();

    try {
      List<HeroModel> heroes = await HeroService().fetchHeroes();
      _heroes = heroes;
      notifyListeners();
    } catch (e) {
      debugPrint(e.toString());
    }

    _isLoading = false;
    notifyListeners();
  }

  Future<void> getHeroesByRole(String role) async {
    _isLoading = true;
    notifyListeners();

    try {
      List<HeroModel> heroes = await HeroService().fetchHeroesByRole(role);
      _heroes = heroes;
      notifyListeners();

      debugPrint(_heroes.toString());
    } catch (e) {
      debugPrint(e.toString());
    }

    _isLoading = false;
    notifyListeners();
  }

  Future<void> getHero(String id) async {
    _isLoadingDetail = true;
    notifyListeners();
    try {
      HeroModel hero = await HeroService().fetchHero(id);
      _hero = hero;

      debugPrint(_heroes.toString());
    } catch (e) {
      debugPrint(e.toString());
    }

    _isLoadingDetail = false;
    notifyListeners();
  }

  void addToFavorites(HeroModel hero) {
    final heroFavorite = HeroFavorite(
      heroId: hero.heroId!,
      heroName: hero.heroName!,
      heroRole: hero.heroRole!,
    );
    heroFavoritesBox.add(heroFavorite);
    notifyListeners();
  }

  void removeFromFavorites(int heroId) {
    heroFavoritesBox.values
        .where((hero) => hero.heroId == heroId)
        .toList()
        .forEach((hero) => hero.delete());
    notifyListeners();
  }

  void setHeroFavorite(HeroModel data) {
    if (isHeroFavorite(data.heroId!)) {
      removeFromFavorites(data.heroId!);
    } else {
      addToFavorites(hero);
    }
  }

  bool isHeroFavorite(int heroId) {
    return heroFavoritesBox.values.any((hero) => hero.heroId == heroId);
  }

  List<HeroFavorite> getFavoriteHeroes() {
    return heroFavoritesBox.values.toList();
  }
}
