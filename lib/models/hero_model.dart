class HeroModel {
  final int? heroId;
  final String? heroName;
  final String? heroAvatar;
  final List<String>? heroRole;
  final List<String>? heroSpecially;
  final HeroOverview? heroOverview;

  HeroModel({
    this.heroId,
    this.heroName,
    this.heroAvatar,
    this.heroRole,
    this.heroSpecially,
    this.heroOverview,
  });

  factory HeroModel.fromJson(Map<String, dynamic> json) {
    return HeroModel(
      heroId: json['hero_id'],
      heroName: json['hero_name'],
      heroAvatar: json['hero_avatar'],
      heroRole: (json['hero_role'] as String).split(','),
      heroSpecially: (json['hero_specially'] as String).split(','),
      heroOverview: json['hero_overview'] != null
          ? HeroOverview.fromJson(json['hero_overview'])
          : HeroOverview(
              heroDurability: 0,
              heroOffence: 0,
              heroAbility: 0,
              heroDifficulty: 0,
            ),
    );
  }
}

class HeroOverview {
  final int heroDurability;
  final int heroOffence;
  final int heroAbility;
  final int heroDifficulty;

  HeroOverview({
    required this.heroDurability,
    required this.heroOffence,
    required this.heroAbility,
    required this.heroDifficulty,
  });

  factory HeroOverview.fromJson(Map<String, dynamic> json) {
    return HeroOverview(
      heroDurability: json['hero_durability'],
      heroOffence: json['hero_offence'],
      heroAbility: json['hero_ability'],
      heroDifficulty: json['hero_difficulty'],
    );
  }
}
