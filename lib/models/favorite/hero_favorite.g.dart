// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hero_favorite.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class HeroFavoriteAdapter extends TypeAdapter<HeroFavorite> {
  @override
  final int typeId = 1;

  @override
  HeroFavorite read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return HeroFavorite(
      heroId: fields[0] as int,
      heroName: fields[1] as String,
      heroRole: (fields[2] as List).cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, HeroFavorite obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.heroId)
      ..writeByte(1)
      ..write(obj.heroName)
      ..writeByte(2)
      ..write(obj.heroRole);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is HeroFavoriteAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
