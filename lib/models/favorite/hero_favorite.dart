import 'package:hive/hive.dart';

part 'hero_favorite.g.dart';

@HiveType(typeId: 1)
class HeroFavorite extends HiveObject {
  @HiveField(0)
  late int heroId;

  @HiveField(1)
  late String heroName;

  @HiveField(2)
  late List<String> heroRole;

  HeroFavorite(
      {required this.heroId, required this.heroName, required this.heroRole});
}
