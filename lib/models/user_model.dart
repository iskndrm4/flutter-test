import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  final String? fullName;
  final String? email;
  final String? gender;
  final DateTime? birthDate;
  final String? province;
  final String? profilePhoto;

  UserModel({
    this.fullName,
    this.email,
    this.gender,
    this.birthDate,
    this.province,
    this.profilePhoto,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      fullName: json['full_name'],
      email: json['email'],
      gender: json['gender'],
      birthDate: (json['birth_date'] as Timestamp).toDate(),
      province: json['province'],
      profilePhoto: json['profile_photo'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'full_name': fullName,
      'email': email,
      'gender': gender!,
      'birth_date': birthDate!.toIso8601String(),
      'province': province,
      'profile_photo': profilePhoto,
    };
  }
}
