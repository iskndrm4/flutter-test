import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile_legend/pages/widget/heroes_card.dart';
import 'package:mobile_legend/providers/hero_provider.dart';
import 'package:mobile_legend/providers/profile_provider.dart';
import 'package:mobile_legend/services/hero_service_dio.dart';
import 'package:mobile_legend/utils/theme.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    Future.microtask(() {
      Provider.of<HeroProvider>(context, listen: false).getHeroes();
      HeroService().fetchHeroesByRole('Marksman');
    });
    super.initState();
  }

  List<String> roles = [
    'Semua',
    'Tank',
    'Fighter',
    'Assassin',
    'Mage',
    'Marksman',
    'Support',
  ];

  @override
  Widget build(BuildContext context) {
    final profile = Provider.of<ProfileProvider>(context);
    Widget header() {
      return Container(
        margin: const EdgeInsets.only(
            top: defaultMargin, left: defaultMargin, right: defaultMargin),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    profile.isLoading == true
                        ? 'Hallo, .......'
                        : 'Hallo, ${profile.userModel.fullName!.split(" ")[0]}',
                    style: primaryTextStyle.copyWith(
                        fontSize: 21, fontWeight: semiBold),
                  ),
                  Text(
                    profile.isLoading == true
                        ? '@ .......'
                        : '@${profile.userModel.fullName!.split(" ")[0]}',
                    style: subtitleTextStyle.copyWith(
                        fontSize: 16, fontWeight: regular),
                  )
                ],
              ),
            ),
            if (profile.isLoading == true)
              Container(
                width: 54,
                height: 54,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/image_profile.png'),
                  ),
                ),
              )
            else
              CachedNetworkImage(
                imageUrl: profile.userModel.profilePhoto ?? '',
                imageBuilder: (context, imageProvider) => Container(
                  width: 55,
                  height: 55,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: primaryTextColor,
                      width: 1.5,
                    ),
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  width: 54,
                  height: 54,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage('assets/image_profile.png'),
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Container(
                  width: 54,
                  height: 54,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage('assets/image_profile.png'),
                    ),
                  ),
                ),
              )
          ],
        ),
      );
    }

    Widget rolesWidegt() {
      return Container(
        height: 40,
        margin: const EdgeInsets.only(
          top: defaultMargin,
        ),
        child: ListView.builder(
          padding: const EdgeInsets.only(left: defaultMargin),
          itemCount: roles.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            final role = roles[index];
            return RoleItem(
              role: role,
              index: index,
            );
          },
        ),
      );
    }

    Widget heroesCard() {
      return Container(
        margin: const EdgeInsets.only(top: 230, left: defaultMargin),
        child: Consumer<HeroProvider>(
          builder: (context, data, _) {
            if (data.isLoading == true) {
              return const Padding(
                padding: EdgeInsets.only(top: 50),
                child: Center(
                  child: CircularProgressIndicator(
                    color: backgroudColor6,
                  ),
                ),
              );
            } else {
              return ListView.builder(
                physics: const BouncingScrollPhysics(),
                itemCount: data.heroes.length,
                itemBuilder: (context, index) {
                  final hero = data.heroes[index];
                  return HeroesCard(
                    hero: hero,
                  );
                },
              );
            }
          },
        ),
      );
    }

    return SafeArea(
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              header(),
              rolesWidegt(),
              Padding(
                padding: const EdgeInsets.only(
                    top: defaultMargin, left: defaultMargin),
                child: Text(
                  'Heroes',
                  style: primaryTextStyle.copyWith(
                      fontSize: 22, fontWeight: semiBold),
                ),
              )
            ],
          ),
          heroesCard()
        ],
      ),
    );
  }
}

class RoleItem extends StatelessWidget {
  final String role;
  final int index;
  const RoleItem({
    Key? key,
    required this.role,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<HeroProvider>(
      builder: (context, roleProvider, _) {
        final isTap = roleProvider.clickedIndex == index;
        return GestureDetector(
          onTap: () {
            roleProvider.toggleTapState(index, role);
          },
          child: Container(
            margin: const EdgeInsets.only(right: 16),
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: isTap ? primaryColor : transparentColor,
              border: isTap ? null : Border.all(color: subtitleTextColor),
            ),
            child: Text(
              role,
              style: isTap
                  ? primaryTextStyle.copyWith(fontSize: 13, fontWeight: medium)
                  : subtitleTextStyle.copyWith(fontSize: 13, fontWeight: light),
            ),
          ),
        );
      },
    );
  }
}
