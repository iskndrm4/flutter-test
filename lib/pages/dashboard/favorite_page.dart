import 'package:flutter/material.dart';
import 'package:mobile_legend/pages/widget/favorite_card.dart';
import 'package:mobile_legend/providers/hero_provider.dart';
import 'package:mobile_legend/utils/theme.dart';
import 'package:provider/provider.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HeroProvider heroProvider = Provider.of<HeroProvider>(context);

    PreferredSizeWidget header() {
      return AppBar(
        title: const Text('Hero Favorit'),
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: backgroudColor1,
      );
    }

    Widget emptyFavorite() {
      return Expanded(
        child: Container(
          width: double.infinity,
          color: backgroudColor3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/image_favorite.png',
                width: 80,
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                'Ayo tambahkan hero favoritmu .. !',
                style:
                    primaryTextStyle.copyWith(fontSize: 16, fontWeight: medium),
              ),
            ],
          ),
        ),
      );
    }

    Widget content() {
      return Expanded(
        child: Container(
          width: double.infinity,
          color: backgroudColor3,
          child: ListView.builder(
            padding: const EdgeInsets.symmetric(horizontal: defaultMargin),
            itemCount: heroProvider.getFavoriteHeroes().length,
            itemBuilder: (context, index) {
              final hero = heroProvider.getFavoriteHeroes()[index];
              return FavoriteCard(favorite: hero);
            },
          ),
        ),
      );
    }

    return Column(children: [
      header(),
      heroProvider.getFavoriteHeroes().isEmpty ? emptyFavorite() : content()
    ]);
  }
}
