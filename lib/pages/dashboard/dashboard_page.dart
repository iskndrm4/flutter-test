import 'package:flutter/material.dart';
import 'package:mobile_legend/pages/dashboard/favorite_page.dart';
import 'package:mobile_legend/pages/dashboard/home_page.dart';
import 'package:mobile_legend/pages/dashboard/profile_page.dart';
import 'package:mobile_legend/providers/dashboard_provider.dart';
import 'package:mobile_legend/providers/profile_provider.dart';
import 'package:mobile_legend/utils/theme.dart';
import 'package:provider/provider.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  void initState() {
    Future.microtask(() {
      Provider.of<ProfileProvider>(context, listen: false)
          .getUserDataFromFirestore();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DashboardProvider dashboardProvider =
        Provider.of<DashboardProvider>(context);

    Widget customBottomNav() {
      return ClipRRect(
        borderRadius:
            const BorderRadius.vertical(top: Radius.circular(defaultMargin)),
        child: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          notchMargin: 12,
          clipBehavior: Clip.antiAlias,
          child: BottomNavigationBar(
            backgroundColor: backgroudColor4,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            currentIndex: dashboardProvider.currentIndex,
            onTap: (value) {
              setState(() {
                dashboardProvider.currentIndex = value;
              });
            },
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                  icon: Container(
                    margin: const EdgeInsets.only(top: 15, bottom: 15),
                    child: Image.asset(
                      'assets/icon_home.png',
                      width: 21,
                      color: dashboardProvider.currentIndex == 0
                          ? primaryColor
                          : const Color(0xff808191),
                    ),
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  icon: Container(
                    margin: const EdgeInsets.only(top: 15, bottom: 15),
                    child: Image.asset(
                      'assets/icon_favorite.png',
                      width: 20,
                      color: dashboardProvider.currentIndex == 1
                          ? primaryColor
                          : const Color(0xff808191),
                    ),
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  icon: Container(
                    margin: const EdgeInsets.only(
                      top: 15,
                      bottom: 15,
                    ),
                    child: Image.asset(
                      'assets/icon_profile.png',
                      width: 18,
                      color: dashboardProvider.currentIndex == 2
                          ? primaryColor
                          : const Color(0xff808191),
                    ),
                  ),
                  label: ''),
            ],
          ),
        ),
      );
    }

    Widget body() {
      switch (dashboardProvider.currentIndex) {
        case 0:
          return const HomePage();
        case 1:
          return const FavoritePage();
        case 2:
          return const ProfilePage();

        default:
          return const HomePage();
      }
    }

    return Scaffold(
      backgroundColor: dashboardProvider.currentIndex == 0
          ? backgroudColor1
          : backgroudColor3,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: customBottomNav(),
      body: body(),
    );
  }
}
