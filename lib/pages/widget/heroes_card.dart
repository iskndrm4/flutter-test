import 'package:flutter/material.dart';
import 'package:mobile_legend/models/hero_model.dart';
import 'package:mobile_legend/pages/detail_hero.dart';
import 'package:mobile_legend/utils/theme.dart';

class HeroesCard extends StatelessWidget {
  final HeroModel hero;
  const HeroesCard({Key? key, required this.hero}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailHero(
            idHero: hero.heroId.toString(),
          ),
        ),
      ),
      child: Container(
        margin: const EdgeInsets.only(bottom: defaultMargin),
        child: Row(
          children: [
            Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                color: backgroudColor6,
                borderRadius: BorderRadius.circular(20),
                image: const DecorationImage(
                  image: AssetImage('assets/logo_mlbb.png'),
                ),
              ),
            ),
            const SizedBox(
              width: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  hero.heroName ?? '',
                  style: primaryTextStyle.copyWith(
                      fontSize: 16,
                      fontWeight: semiBold,
                      overflow: TextOverflow.ellipsis),
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  hero.heroRole != null ? hero.heroRole!.join(', ') : '',
                  style: secondaryTextStyle.copyWith(fontWeight: medium),
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  hero.heroSpecially != null
                      ? hero.heroSpecially!.join(', ')
                      : '',
                  style:
                      blueTextStyle.copyWith(fontWeight: medium, fontSize: 12),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
