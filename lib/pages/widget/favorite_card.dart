import 'package:flutter/material.dart';
import 'package:mobile_legend/models/favorite/hero_favorite.dart';
import 'package:mobile_legend/pages/detail_hero.dart';
import 'package:mobile_legend/providers/hero_provider.dart';
import 'package:mobile_legend/utils/theme.dart';
import 'package:provider/provider.dart';

class FavoriteCard extends StatelessWidget {
  final HeroFavorite favorite;
  const FavoriteCard({
    Key? key,
    required this.favorite,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HeroProvider heroProvider = Provider.of<HeroProvider>(context);
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailHero(
            idHero: favorite.heroId.toString(),
          ),
        ),
      ),
      child: Container(
        margin: const EdgeInsets.only(top: 25),
        padding: const EdgeInsets.fromLTRB(12, 10, 20, 14),
        decoration: BoxDecoration(
            color: backgroudColor4, borderRadius: BorderRadius.circular(12)),
        child: Row(
          children: [
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                color: backgroudColor6,
                borderRadius: BorderRadius.circular(12),
                image: const DecorationImage(
                  image: AssetImage('assets/logo_mlbb.png'),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    favorite.heroName,
                    style: primaryTextStyle.copyWith(fontWeight: semiBold),
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: 2,
                  ),
                  Text(
                    favorite.heroRole.join(', '),
                    style: blueTextStyle.copyWith(fontWeight: semiBold),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                heroProvider.removeFromFavorites(favorite.heroId);
              },
              child: Image.asset(
                heroProvider.isHeroFavorite(favorite.heroId)
                    ? 'assets/button_favorite_blue.png'
                    : 'assets/button_favorite.png',
                width: 34,
              ),
            )
          ],
        ),
      ),
    );
  }
}
