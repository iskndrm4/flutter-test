import 'package:flutter/material.dart';
import 'package:mobile_legend/main.dart';
import 'package:mobile_legend/utils/theme.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    getInit();
    super.initState();
  }

  getInit() async {
    await Future.delayed(const Duration(seconds: 3)).then((_) {
      final isLogin = prefs.getBool('is_login');
      if (isLogin == true) {
        Navigator.pushReplacementNamed(context, '/dashboard');
      } else {
        Navigator.pushReplacementNamed(context, '/signin');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroudColor1,
      body: Center(
        child: Container(
          width: 250,
          height: 200,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/logo_mlbb.png'),
            ),
          ),
        ),
      ),
    );
  }
}
