import 'package:flutter/material.dart';
import 'package:mobile_legend/models/hero_model.dart';
import 'package:mobile_legend/providers/hero_provider.dart';
import 'package:mobile_legend/utils/theme.dart';
import 'package:provider/provider.dart';

class DetailHero extends StatefulWidget {
  final String idHero;
  const DetailHero({
    Key? key,
    required this.idHero,
  }) : super(key: key);

  @override
  State<DetailHero> createState() => _DetailHeroState();
}

class _DetailHeroState extends State<DetailHero> {
  @override
  void initState() {
    Future.microtask(() {
      Provider.of<HeroProvider>(context, listen: false).getHero(widget.idHero);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HeroProvider>(
      builder: (context, data, _) {
        if (data.isLoadingDetail == true) {
          return const Scaffold(
            backgroundColor: backgroudColor1,
            body: Center(
              child: CircularProgressIndicator(
                color: backgroudColor6,
              ),
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: backgroudColor6,
            body: DetailContent(
              heroProvider: data,
              hero: data.hero,
              overview: data.hero.heroOverview!,
            ),
          );
        }
      },
    );
  }
}

class DetailContent extends StatelessWidget {
  final HeroProvider heroProvider;
  final HeroModel hero;
  final HeroOverview overview;
  const DetailContent({
    super.key,
    required this.hero,
    required this.overview,
    required this.heroProvider,
  });

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding:
            const EdgeInsets.fromLTRB(20, defaultMargin, defaultMargin, 20),
        child: Image.asset(
          'assets/logo_mlbb.png',
        ),
      ),
      Expanded(
        child: Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            color: backgroudColor1,
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(20),
            ),
          ),
          child: ListView(
            // crossAxisAlignment: CrossAxisAlignment.start,
            physics: const BouncingScrollPhysics(),
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    defaultMargin, 0, defaultMargin, 0),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            hero.heroName ?? '',
                            style: primaryTextStyle.copyWith(
                                fontSize: 18, fontWeight: semiBold),
                          ),
                          Text(
                            hero.heroRole != null
                                ? hero.heroRole!.join(', ')
                                : '',
                            style: secondaryTextStyle.copyWith(fontSize: 12),
                          )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        heroProvider.setHeroFavorite(hero);
                        if (heroProvider.isHeroFavorite(hero.heroId!)) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              backgroundColor: secondaryColor,
                              content: Text(
                                'Berhasil ditambahkan ke favorite',
                                textAlign: TextAlign.center,
                              ),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              backgroundColor: alertColor,
                              content: Text(
                                'Berhasil dihapus dari favorit',
                                textAlign: TextAlign.center,
                              ),
                            ),
                          );
                        }
                      },
                      child: Image.asset(
                        heroProvider.isHeroFavorite(hero.heroId!)
                            ? 'assets/button_favorite_blue.png'
                            : 'assets/button_favorite.png',
                        width: 46,
                      ),
                    ),
                  ],
                ),
              ),

              // Specialy
              Container(
                margin: const EdgeInsets.fromLTRB(
                    defaultMargin, 20, defaultMargin, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Hero Spesial',
                      style: primaryTextStyle.copyWith(fontWeight: medium),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Column(
                      children: hero.heroSpecially != null
                          ? hero.heroSpecially!
                              .map((e) => SpeciallyItem(title: e))
                              .toList()
                          : [],
                    ),
                  ],
                ),
              ),

              // Specialy
              Padding(
                padding: const EdgeInsets.fromLTRB(
                    defaultMargin, 10, defaultMargin, defaultMargin),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Ringkasan Hero',
                      style: primaryTextStyle.copyWith(fontWeight: medium),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    OverviewItem(
                      title: 'Hero Durability',
                      value: overview.heroDurability.toDouble(),
                    ),
                    OverviewItem(
                      title: 'Hero Offence',
                      value: overview.heroOffence.toDouble(),
                    ),
                    OverviewItem(
                      title: 'Hero Ability',
                      value: overview.heroAbility.toDouble(),
                    ),
                    OverviewItem(
                      title: 'Hero Difficulty',
                      value: overview.heroDifficulty.toDouble(),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )
    ]);
  }
}

class SpeciallyItem extends StatelessWidget {
  final String title;
  const SpeciallyItem({
    super.key,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          const Icon(
            Icons.circle,
            size: 8,
            color: blueColor,
          ),
          const SizedBox(width: 10),
          Text(
            title,
            style: secondaryTextStyle.copyWith(
              fontWeight: light,
            ),
          ),
        ],
      ),
    );
  }
}

class OverviewItem extends StatelessWidget {
  final String title;
  final double value;
  const OverviewItem({
    super.key,
    required this.title,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: Column(
        children: [
          Row(
            children: [
              const Icon(
                Icons.circle,
                size: 8,
                color: blueColor,
              ),
              const SizedBox(width: 10),
              Text(
                '$title :',
                style: secondaryTextStyle.copyWith(
                  fontWeight: light,
                ),
              ),
              const SizedBox(width: 10),
              Text(
                value.toString(),
                style: secondaryTextStyle.copyWith(
                  fontWeight: light,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          LinearProgressIndicator(
            value: value / 100,
            backgroundColor: Colors.grey[300],
            valueColor: const AlwaysStoppedAnimation<Color>(Colors.blue),
          ),
        ],
      ),
    );
  }
}
