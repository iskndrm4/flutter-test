import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mobile_legend/database/hive_helper.dart';
import 'package:mobile_legend/pages/dashboard/dashboard_page.dart';
import 'package:mobile_legend/pages/dashboard/favorite_page.dart';
import 'package:mobile_legend/pages/login_page.dart';
import 'package:mobile_legend/pages/signup_page.dart';
import 'package:mobile_legend/pages/splash_page.dart';
import 'package:mobile_legend/providers/auth_provider.dart';
import 'package:mobile_legend/providers/dashboard_provider.dart';
import 'package:mobile_legend/providers/hero_provider.dart';
import 'package:mobile_legend/providers/profile_provider.dart';
import 'package:mobile_legend/utils/certificate.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'firebase_options.dart';

late SharedPreferences prefs;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  prefs = await SharedPreferences.getInstance();

  HttpOverrides.global = MyHttpOverrides();

  await initializeDateFormatting('id_ID', null);

  HiveHelper.initializeHive();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => DashboardProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProfileProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => HeroProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => const SplashPage(),
          '/signin': (context) => const LoginPage(),
          '/signup': (context) => const SignUpPage(),
          '/dashboard': (context) => const DashboardPage(),
          '/favorite': (context) => const FavoritePage(),
        },
      ),
    );
  }
}
