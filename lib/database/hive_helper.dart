import 'package:hive_flutter/hive_flutter.dart';
import 'package:mobile_legend/models/favorite/hero_favorite.dart';

class HiveHelper {
  const HiveHelper._();

  static Future<void> initializeHive() async {
    await Hive.initFlutter();
    Hive.registerAdapter(HeroFavoriteAdapter());
    await Hive.openBox<HeroFavorite>('heroFavorites');
  }
}
