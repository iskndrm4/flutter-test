// import 'dart:convert';
// import 'package:http/http.dart' as http;
// import 'package:mobile_legend/models/hero_model.dart';

// class HeroService {
//   String baseUrl = 'https://api.dazelpro.com/mobile-legends/';

//   Future<List<HeroModel>> fetchHeroes() async {
//     final response = await http.get(Uri.parse('$baseUrl/hero'));

//     if (response.statusCode == 200) {
//       final jsonData = json.decode(response.body);
//       final List<dynamic> heroList = jsonData['hero'];

//       List<HeroModel> heroes =
//           heroList.map((json) => HeroModel.fromJson(json)).toList();

//       return heroes;
//     } else {
//       throw Exception('Failed to fetch heroes');
//     }
//   }

//   Future<List<HeroModel>> fetchHeroesByRole(String role) async {
//     final response = await http.get(Uri.parse('$baseUrl/role?roleName=$role'));

//     if (response.statusCode == 200) {
//       final jsonData = json.decode(response.body);
//       final List<dynamic> heroList = jsonData['hero'];

//       List<HeroModel> heroes =
//           heroList.map((json) => HeroModel.fromJson(json)).toList();

//       return heroes;
//     } else {
//       throw Exception('Failed to fetch heroes');
//     }
//   }

//   Future<HeroModel> fetchHero(String id) async {
//     final response = await http.get(Uri.parse('$baseUrl/hero/$id'));

//     if (response.statusCode == 200) {
//       final jsonData = json.decode(response.body);
//       final data = jsonData['hero'];

//       HeroModel hero = HeroModel.fromJson(data[0] as Map<String, dynamic>);

//       return hero;
//     } else {
//       throw Exception('Failed to fetch hero');
//     }
//   }
// }
