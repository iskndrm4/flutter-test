import 'dart:convert';
import 'package:http/http.dart' as http;

class ProvinceService {
  String baseUrl =
      'https://www.emsifa.com/api-wilayah-indonesia/api/provinces.json';

  Future<List<String>> fetchProvinces() async {
    final response = await http.get(Uri.parse(baseUrl));

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final List<String> provinces = [];

      for (var province in jsonData) {
        provinces.add(province['name']);
      }

      return provinces;
    } else {
      throw Exception('Failed to fetch provinces');
    }
  }
}
