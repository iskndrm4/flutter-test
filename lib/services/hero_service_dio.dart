import 'dart:io';

import 'package:mobile_legend/models/hero_model.dart';
import 'package:dio/dio.dart';

class HeroService {
  String baseUrl = 'https://api.dazelpro.com/mobile-legends/';
  final dio = Dio();

  Future<List<HeroModel>> fetchHeroes() async {
    try {
      final response = await dio.get('$baseUrl/hero');

      if (response.statusCode == 200) {
        final jsonData = response.data;
        final List<dynamic> heroList = jsonData['hero'];

        List<HeroModel> heroes =
            heroList.map((json) => HeroModel.fromJson(json)).toList();

        return heroes;
      } else {
        throw Exception('Failed to fetch heroes');
      }
    } on DioException catch (e) {
      return Future.error('An error occurred during the request ($e)');
    } on HandshakeException catch (e) {
      return Future.error('Connection terminated during handshake ($e)');
    } catch (e) {
      return Future.error('An error occurred while fetching heroes');
    }
  }

  Future<List<HeroModel>> fetchHeroesByRole(String role) async {
    try {
      final response = await dio.get('$baseUrl/role?roleName=$role');

      if (response.statusCode == 200) {
        final jsonData = response.data;
        final List<dynamic> heroList = jsonData['hero'];

        List<HeroModel> heroes =
            heroList.map((json) => HeroModel.fromJson(json)).toList();

        return heroes;
      } else {
        throw Exception('Failed to fetch heroes');
      }
    } on DioException catch (e) {
      return Future.error('An error occurred during the request ($e)');
    } on HandshakeException catch (e) {
      return Future.error('Connection terminated during handshake ($e)');
    } catch (e) {
      return Future.error('An error occurred while fetching heroes');
    }
  }

  Future<HeroModel> fetchHero(String id) async {
    try {
      final response = await dio.get('$baseUrl/hero/$id');

      if (response.statusCode == 200) {
        final jsonData = response.data;
        final data = jsonData['hero'];

        HeroModel hero = HeroModel.fromJson(data[0] as Map<String, dynamic>);

        return hero;
      } else {
        throw Exception('Failed to fetch hero');
      }
    } on DioException catch (e) {
      return Future.error('An error occurred during the request ($e)');
    } on HandshakeException catch (e) {
      return Future.error('Connection terminated during handshake ($e)');
    } catch (e) {
      return Future.error('An error occurred while fetching hero');
    }
  }
}
